﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadastroDePessoas_v._1.ws;
using static System.Net.Mime.MediaTypeNames;
using System.Drawing;
using System.IO;

namespace CadastroDePessoas_v._1
{
    class Program
    {



        static void Main(string[] args)
        {
            endereco endereco = new endereco();
            endereco.id = 1111;
            endereco.idSpecified = true;
            endereco.rua = "Rua C";
            endereco.numero = "3333";

            String path = Environment.CurrentDirectory + "/paloci.jpg";
            Bitmap b = new Bitmap(path);

            pessoa pessoa = new pessoa();
            pessoa.endereco = endereco;
            pessoa.id = 9999;
            pessoa.foto = ImageToByte2(b);
            pessoa.idSpecified = true;
            pessoa.nome = "João";

            CadastroWSClient cadastro = new CadastroWSClient();
            cadastro.salvar(pessoa);
        }

        public static byte[] ImageToByte2(System.Drawing.Image img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }
    }
}
